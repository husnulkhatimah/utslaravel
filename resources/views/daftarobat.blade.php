<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/javascript" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="gaya.css">
    <title>Apotik Citra Medika</title>
    
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      
      
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link mr-4" aria-current="page" href="/" role="button">HOME</a>
          </li>
          <li class="nav-item">
            <a class="nav-link mr-4" href="saya" role="button">DAFTAR OBAT</a>
          </li>
          <li class="nav-item">
            <a class="nav-link mr-4" href="{{ route('pesan')}}" role="button">PESANAN</a>
          </li>
        </ul>
      </div>
    </div>
</nav>

  
    
    <div class="container-fluid">
        <div class="row">
           <div class="col-md-2">
                <div class="card border-dark">
                 <img src="image/angin.jpg" class="card-img-top" alt="...">
                 <div class="card-body">
                     <h5 class="card-title font-weight-bold">Tolak Angin</h5>
                     <label class="card-text harga">Rp. 20.000</label><br>
                 </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card border-dark">
                <img src="image/betadin.jpg" class="card-img-top" alt="...">
                  <div class="card-body">
                      <h5 class="card-title font-weight-bold">Betadin</h5>
                      <label class="card-text harga">Rp. 16.000</label><br>
                  </div>
                </div>
        </div>

        <div class="col-md-2">
             <div class="card border-dark">
            <img src="image/kalpanak.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                  <h5 class="card-title font-weight-bold">Kalpanak</h5>
                  <label class="card-text harga">Rp. 12.000</label><br>
              </div>
             </div>
         </div>

        <div class="col-md-2">
             <div class="card border-dark">
            <img src="image/bodrek.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                  <h5 class="card-title font-weight-bold">Bodrex</h5>
                  <label class="card-text harga">Rp. 7.000</label><br>
              </div>
             </div>
        </div>

        <div class="col-md-2">
            <div class="card border-dark">
           <img src="image/insto.jpg" class="card-img-top" alt="...">
             <div class="card-body">
                 <h5 class="card-title font-weight-bold">Cataflan</h5>
                 <label class="card-text harga">Rp. 10.000</label><br>
             </div>
            </div>
       </div>
       <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/colme.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Colme</h5>
             <label class="card-text harga">Rp. 18.000</label><br>
         </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/combi.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Obh combi</h5>
             <label class="card-text harga">Rp. 15.000</label><br>
         </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/combi2.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Obh Combi Anak</h5>
             <label class="card-text harga">Rp. 18.000</label><br>
         </div>
        </div>
      </div>
      <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/kalpanakcair.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Kalpanak Cair</h5>
         <label class="card-text harga">Rp. 14.000</label><br>
     </div>
    </div>
  </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/erla.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Erla Mycetin</h5>
             <label class="card-text harga">Rp. 17.000</label><br>
         </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/feminax.jpeg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Feminax</h5>
             <label class="card-text harga">Rp. 3.000</label><br>
         </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/formula.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Formula 44</h5>
             <label class="card-text harga">Rp. 15.000</label><br>
         </div>
        </div>
      </div>
   <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/fres1.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Freshcare citrus</h5>
         <label class="card-text harga">Rp. 14.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/fres2.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Freshcare Strog</h5>
         <label class="card-text harga">Rp. 9.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/fres3.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Frashcare Lavender</h5>
         <label class="card-text harga">Rp. 8.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/fres4.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Freshcare Rol On</h5>
         <label class="card-text harga">Rp. 10.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/fres6.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Freshcare</h5>
         <label class="card-text harga">Rp. 8.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/ikamicetin.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Ikamicetin</h5>
         <label class="card-text harga">Rp. 25.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/cataflam.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Cataflam</h5>
         <label class="card-text harga">Rp. 10.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/bisolvon.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Bisolvo</h5>
         <label class="card-text harga">Rp. 10.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/diapet.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Diapet</h5>
             <label class="card-text harga">Rp. 8.000</label><br>
         </div>
        </div>
      </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/kapsida.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Kapsida</h5>
         <label class="card-text harga">Rp. 8.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/kayuputih.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Minyak Kayu Putih</h5>
         <label class="card-text harga">Rp. 17.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/kb.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Pil Kb</h5>
         <label class="card-text harga">Rp. 5.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/kurma.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Sari Kurma</h5>
         <label class="card-text harga">Rp. 20.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/lasal.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Sirup Lasal</h5>
         <label class="card-text harga">Rp. 9.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/linu.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Tolak Linu</h5>
         <label class="card-text harga">Rp. 10.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/madu.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Madu Tj</h5>
         <label class="card-text harga">Rp. 25.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/molex.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Molexdryl</h5>
         <label class="card-text harga">Rp. 12.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/nature.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Natur E</h5>
         <label class="card-text harga">Rp. 150.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/neu2.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Neurobion Forte</h5>
         <label class="card-text harga">Rp. 25.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/neuralgin.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Neuralgin</h5>
         <label class="card-text harga">Rp. 8.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/ob.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Ob Herbal</h5>
         <label class="card-text harga">Rp. 12.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/omix.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Omegtamine</h5>
         <label class="card-text harga">Rp. 7.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/oralit.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Oralit</h5>
         <label class="card-text harga">Rp. 6.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/paracetamol.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Paracetamol</h5>
         <label class="card-text harga">Rp. 10.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/pikang.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Pikangsuang</h5>
         <label class="card-text harga">Rp. 7.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/pikangsuang.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Pikangsuang</h5>
         <label class="card-text harga">Rp. 7.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/polysilane.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Polisylane</h5>
         <label class="card-text harga">Rp. 20.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/pro.jpeg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Prolis Sm</h5>
         <label class="card-text harga">Rp. 70.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/salep.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Salep 88</h5>
         <label class="card-text harga">Rp. 25.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/sanbe.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Astharol</h5>
         <label class="card-text harga">Rp. 9.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/sango.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Sangobion</h5>
         <label class="card-text harga">Rp. 7.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/sindo.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Cendro Sitrol</h5>
         <label class="card-text harga">Rp. 15.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/telon.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Minyak Telon</h5>
         <label class="card-text harga">Rp. 10.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/ultrasiline.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Ultrasiline</h5>
         <label class="card-text harga">Rp. 20.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/vik.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Vicks</h5>
         <label class="card-text harga">Rp. 10.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/vital.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Vital</h5>
         <label class="card-text harga">Rp. 9.000</label><br>
     </div>
    </div>
  </div>
</div>      
   
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp80k3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpgZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src=""></script>
</body>
</html>