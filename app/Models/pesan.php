<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pesan extends Model
{
    protected $table ="pesans";
    protected $primariKey ="id";
    protected $fillable = [
        'id','nama_barang','harga_barang','jumlah_pesanan','tanggal'
    ];
}
